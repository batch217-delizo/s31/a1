// 1. What directive is used by Node.js in loading the modules it needs?
/* 
  require();
// The require() method is used to load and cache JavaScript modules.
// So, if you want to load a local, relative JavaScript module into a Node.js application,
// you can simply use the require() method

*/


// 2. What Node.js module contains a method for server creation?
/* 
http module
The http module contains the function to create the server, which we will see later on. 

*/


// 3. What is the method of the http object responsible for creating a server using Node.js?

/* 

The http.createServer() method turns your computer into an HTTP server.

The http.createServer() method creates an HTTP Server object.

*/


// 4. What method of the response object allows us to set status codes and content types?
	
/* 
The first argument of the res.writeHead() method is the status code, 200 means that all is OK, 
404 is page not found, the second argument is an object containing the response headers.
*/


// 5. Where will console.log() output its contents when run in Node.js?

/* 

-- it prints the string or varaible data you pass to console and display a message in terminal

-- The console.log() function from console class of Node.js is used to display
 the messages on the console. It prints to stdout with newline.

*/

// 6. What property of the request object contains the address's endpoint?

/* 
The request object represents the HTTP request and has properties for the
 request query string, parameters, body, HTTP headers, and so on.

*/
